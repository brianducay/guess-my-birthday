from random import randint

name = input('Hi! What is your name?')
count = 0

def game():
        global count
        if count >=4:
            print("I have other things to do. Good bye.")
            return

        month = randint(0, 13)
        year = randint(1924, 2004)
        answer = input(f"{name} were you born in {month}/{year}? yes or no?")
        if answer.lower() == "yes":
            print("I knew it!")
            return
        elif answer.lower() == "no":
            print("Drat! Let me try again!")
            count += 1
            game()
        else:
            print("please enter yes or no")
            count += 1
            game()
game()
