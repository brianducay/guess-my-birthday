from random import randint

name = input('Hi! What is your name?')
count = 0

def game():

    for i in range(4):
        month = randint(0, 13)
        year = randint(1924, 2004)
        answer = input(f"{name} were you born in {month}/{year}? yes or no?")
        if answer.lower() == "yes":
            print("I knew it!")
            break
        elif answer.lower() == "no":
            print("Drat! Lemme try again!")
        else:
            print("please enter yes or no")

    print("I have other things to do. Good bye.")
    return
game()
